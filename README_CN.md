# java-legendary

#### 介绍
Java超神之路

#### 面试高频考点

- [JVM](面试题/jvm/超神之路-JVM.md)
- 多线程
- [Redis](面试题/数据库/Redis/超神之路-Redis.md)
- [MySQL](面试题/数据库/MySQL/超神之路-MySQL.md)
- 核心框架源码
- [分布式相关(eg:分布式id、分布式事务、限流算法、哈希一致性等分布式场景以及分布式常见问题)](面试题/分布式相关/超神之路-分布式相关.md)
- [RocketMQ](面试题/中间件/RocketMQ/超神之路-RocketMQ.md)
- [tomcat](面试题/tomcat/超神之路-Tomcat.md)
- io/nio/netty
- [docker](面试题/云原生/docker/超神之路-docker.md)
- [k8s](https://www.kubernetes.org.cn/5578.html)
- 网络协议
- [Linux提升工作效率的高频命令](面试题/linux/超神之路-Linux提升工作效率的高频命令.md)
- elasticsearch
- [istio](面试题/istio/超神之路-istio.md)
- 算法
- 项目

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request
